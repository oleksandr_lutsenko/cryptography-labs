import lab1

import matplotlib.pyplot as plt
import collections
import numpy as np

def show_stats_letters(encrypted, unencrypted):

    letters_dict_encrypted = get_letters(encrypted)
    letters_dict_unencrypted = get_letters(unencrypted)
    
    show(letters_dict_encrypted, letters_dict_unencrypted)

def show_stats_letters_alphabet(encrypted, unencrypted):

    letters_dict_alphabet_encrypted = get_letters_by_alphabet(encrypted)
    letters_dict_alphabet_unencrypted = get_letters_by_alphabet(unencrypted)
    
    show_on_one_graph(letters_dict_alphabet_encrypted, letters_dict_alphabet_unencrypted)

def show_stats_bigrams(encrypted, unencrypted):
    bigrams_dict_encrypted = get_bigrams(encrypted)
    bigrams_dict_unencrypted = get_bigrams(unencrypted)
   
    show(bigrams_dict_encrypted, bigrams_dict_unencrypted)

def show_stats_trigrams(encrypted, unencrypted):
    trigrams_dict_encrypted = get_trigrams(encrypted)
    trigrams_dict_unencrypted = get_trigrams(unencrypted)

    print(trigrams_dict_encrypted)
    print(trigrams_dict_unencrypted)
   
    show(trigrams_dict_encrypted, trigrams_dict_unencrypted)

def get_trigrams(text):
    trigrams = []
    for i in range(len(text) - 2):
        trigrams.append(text[i] + text[i+ 1] + text[i + 2])

    trigrams_dict = lab1.count_keys(trigrams)

    sorted_trigrams = sorted(trigrams_dict.items(), key=lambda kv: kv[1], reverse = True)[:10]
    sorted_trigrams_dict = collections.OrderedDict(sorted_trigrams)

    return sorted_trigrams_dict

def show(encrypted_dict, unencrypted_dict):

    if (len(encrypted_dict.keys()) != len(unencrypted_dict.keys())):
        unencrypted_dict = fix_size_error(unencrypted_dict, encrypted_dict)

    fig, ax = plt.subplots()
    
    x = np.arange(len(encrypted_dict.keys()))
    x_enc = np.arange(len(encrypted_dict.keys()))
    desirable_width = 0.35
    
    encrypted_labels = ['\'{0}\''.format(element) for element in encrypted_dict.keys()]
    unencrypted_labels = ['\'{0}\''.format(element) for element in unencrypted_dict.keys()]

    graph_encrypted = ax.bar(x - desirable_width/2, list(encrypted_dict.values()),desirable_width, color='g', label='Encrypted')
    graph_unencrypted = ax.bar(x_enc + desirable_width/2, list(unencrypted_dict.values()),desirable_width, color='b', label='Unencrypted')

    labels = get_labels(list(encrypted_labels), list(unencrypted_labels))

    ax.set_ylabel('Frequency')
    

    autolabel(graph_encrypted, ax)
    autolabel(graph_unencrypted, ax)


    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    ax.autoscale_view()

    fig.tight_layout()

    mng = plt.get_current_fig_manager()
    mng.window.state('zoomed')
    plt.show()

def show_on_one_graph(encrypted_dict, unencrypted_dict):
    
    if (len(encrypted_dict.keys()) != len(unencrypted_dict.keys())):
        unencrypted_dict = fix_size_error(unencrypted_dict, encrypted_dict)

    
    plt.title('Text Statistical analysis')

    x = np.arange(len(encrypted_dict.keys()))
    desirable_width = 0.35

    letters_fig_encrypted = plt.subplot(2,1,1)
    bar_encrypted = plt.bar(encrypted_dict.keys(), list(encrypted_dict.values()), color='g')
    letters_fig_encrypted.set_xlabel('Letter encrypted')

    letters_fig_unencrypted = plt.subplot(2,1,2)
    bar_unencrypted = plt.bar(unencrypted_dict.keys(), list(unencrypted_dict.values()), color='yellow')
    letters_fig_unencrypted.set_xlabel('Letter unencrypted')

    autolabel(bar_encrypted, letters_fig_encrypted)
    autolabel(bar_unencrypted, letters_fig_unencrypted)

    letters_fig_encrypted.set_xticks(x)
    letters_fig_unencrypted.set_xticks(x)

    plt.tight_layout()

    mng = plt.get_current_fig_manager()
    mng.window.state('zoomed')
    plt.show()




def fix_size_error(unencrypted_dict, encrypted_dict):
    for key in encrypted_dict:
        if (key not in unencrypted_dict):
            unencrypted_dict[key] = 0

    return unencrypted_dict

def get_letters(text):
    alphabet_dict = lab1.count_keys(text)

    sorted_values = sorted(alphabet_dict.items(), key=lambda kv: kv[1], reverse = True)
    sorted_dict = collections.OrderedDict(sorted_values)
    
    return sorted_dict

def get_letters_by_alphabet(text):
    letters_dict = lab1.count_keys(text)

    sorted_by_alphabet_dict = collections.OrderedDict(sorted(letters_dict.items()))

    return sorted_by_alphabet_dict

def get_bigrams(text):
    bigrams = []
    for i in range(len(text) - 1):
        bigrams.append(text[i] + text[i+ 1])

    bigrams_dict = lab1.count_keys(bigrams)

    sorted_bigrams = sorted(bigrams_dict.items(), key=lambda kv: kv[1], reverse = True)[:10]
    sorted_bigrams_dict = collections.OrderedDict(sorted_bigrams)

    bigrams_with_quotes = ['\'{0}\''.format(element) for element in sorted_bigrams_dict.keys()]

    return sorted_bigrams_dict



def get_labels(encrypted_list, unencrypted_list):
    labels = list()

    for i in range(len(encrypted_list) - 1):
        labels.append(encrypted_list[i] + '|' + unencrypted_list[i])

    return labels

def autolabel(rects, ax):
    
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')





