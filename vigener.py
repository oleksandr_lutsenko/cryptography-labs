
def encryptVigener(plain_text, isEncrypting, key):

    keyword = (key * (int(len(plain_text)/len(key)) + 1))[:len(plain_text)]

    changed_text_list = list()

    alphabet = list(' .,;-\'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    counter = 0

    for sym in plain_text:
        if (sym != '\n'):
            if (isEncrypting):
                sym = alphabet[(alphabet.index(sym) + alphabet.index(keyword[counter])) % 32]
            else:
                sym = alphabet[(alphabet.index(sym) - alphabet.index(keyword[counter])) % 32]

        changed_text_list.append(sym)
        counter += 1

    changed_text = ''.join(changed_text_list)

    return changed_text
