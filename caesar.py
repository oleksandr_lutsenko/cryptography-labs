
def readCaesarCypher(contents, shift):
    changed_text_list = list()

    alphabet = list(' .,;-\'ABCDEFGHIJKLMNOPQRSTUVWXYZ')

    for sym in contents:
        if (sym != '\n'):
            sym = alphabet[(alphabet.index(sym) + shift) % len(alphabet)]
        changed_text_list.append(sym)

    changed_text = ''.join(changed_text_list)

    return changed_text
