import matplotlib.pyplot as plt
import collections
import numpy as np


def count_keys(contents):
    alphabet_dict = {}

    for symbol in contents:
        if symbol in alphabet_dict:
            alphabet_dict[symbol] += 1
        else:
            alphabet_dict[symbol] = 1
    return alphabet_dict

def show_symbols_alphabetical(contents):
    alphabet_dict = count_keys(contents)

    sorted_by_alphabet_dict = collections.OrderedDict(sorted(alphabet_dict.items()))
    bar = plt.bar(sorted_by_alphabet_dict.keys(), list(sorted_by_alphabet_dict.values()), color='g')
    plt.xlabel('Symbol')

    yticks = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700]
    show(yticks)

def show_symbols_desc(contents):
    alphabet_dict = count_keys(contents)
    sorted_values = sorted(alphabet_dict.items(), key=lambda kv: kv[1], reverse = True)
    sorted_dict = collections.OrderedDict(sorted_values)

    
    print("{:<8} {:<10}".format('Буква', 'Частота'))
    for k, v in sorted_dict.items():
        print("{:<8} {:<10}".format(k, v))

    bar = plt.bar(sorted_dict.keys(), list(sorted_dict.values()), color='g')
    plt.xlabel('Symbol')

    
    yticks = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700]
    show(yticks)

def show_bigrams(contents):
    bigrams = []
    for i in range(len(contents) - 1):
        bigrams.append(contents[i] + contents[i+ 1])

    bigrams_dict = count_keys(bigrams)

    sorted_bigrams = sorted(bigrams_dict.items(), key=lambda kv: kv[1], reverse = True)[:10]
    sorted_bigrams_dict = collections.OrderedDict(sorted_bigrams)

    bigrams_with_quotes = ['\'{0}\''.format(element) for element in sorted_bigrams_dict.keys()]

    plt.bar(bigrams_with_quotes, list(sorted_bigrams_dict.values()), color='g')
    plt.xlabel('Bigram')
    
    yticks = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110]
    show(yticks)

def show_trigrams(contents):
    trigrams = []
    for i in range(len(contents) - 2):
        trigrams.append(contents[i] + contents[i+ 1] + contents[i + 2])

    trigrams_dict = count_keys(trigrams)

    sorted_trigrams = sorted(trigrams_dict.items(), key=lambda kv: kv[1], reverse = True)[:10]
    sorted_trigrams_dict = collections.OrderedDict(sorted_trigrams)

    trigrams_with_quotes = ['\'{0}\''.format(element) for element in sorted_trigrams_dict.keys()]

    bar = plt.bar(trigrams_with_quotes, list(sorted_trigrams_dict.values()), color='g')
    plt.xlabel('Trigram')

    yticks = [0, 10, 20, 30, 40, 50]
    show(yticks)




def show(yticks):

    plt.yticks(yticks)

    plt.title('Text Statistical analysis')

    plt.tight_layout()

    mng = plt.get_current_fig_manager()
    mng.window.state('zoomed')
    plt.show()

