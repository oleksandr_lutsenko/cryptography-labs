
def readSZCypher(contents, changed_alphabet):
    changed_text_list = list()

    alphabet = list(' .,;-\'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    print(alphabet)
    print(changed_alphabet)

    for sym in contents:
        if (sym != '\n'):
            sym = changed_alphabet[alphabet.index(sym)]
            
        changed_text_list.append(sym)

    changed_text = ''.join(changed_text_list)

    return changed_text

