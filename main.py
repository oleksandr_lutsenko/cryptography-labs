import sys
from PyQt5 import QtGui, QtCore, QtWidgets, Qt

import labsgui
import lab1
import caesar
import sz
import vigener
import cypher_stats

class LabApp(QtWidgets.QMainWindow, labsgui.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.onlyInt = QtGui.QIntValidator(1,32, self)
        self.caesarShift.setValidator(self.onlyInt)

        self.alphabetEdit.setReadOnly(True)

        self.alphabetsTable.resizeColumnsToContents()

        self.setWindowTitle("Encrypting Labs")

        self.showSymbols.clicked.connect(self.showSymbolsAlph)
        self.showDesc.clicked.connect(self.showSymbolsDesc)
        self.showBigrams.clicked.connect(self.showBigrams1)
        self.showTrigrams.clicked.connect(self.showTrigrams1)

        self.caesarBtn.clicked.connect(self.caesarCypher)
        self.statsBtn.clicked.connect(self.caesarStatsLetters)
        self.statsBtnBigram.clicked.connect(self.caesarStatsBigram)
        self.statsBtnTrigram.clicked.connect(self.caesarStatsTrigram)
        self.statsBtnAlphabet.clicked.connect(self.caesarStatsAlphabet)

        self.szBtn.clicked.connect(self.szCypher)
        self.statsBtnSZ.clicked.connect(self.statsLettersSZ)
        self.statsBtnBigramSZ.clicked.connect(self.statsBigramSZ)
        self.statsBtnTrigramSZ.clicked.connect(self.statsTrigramSZ)
        self.statsBtnAlphabetSZ.clicked.connect(self.statsLettersSZAlphabet)


        self.vigenerBtn_encrypt.clicked.connect(self.vigenerEncrypt)
        self.vigenerBtn_unencrypt.clicked.connect(self.vigenerUnencrypt)
        self.statsBtnVigener.clicked.connect(self.statsLettersVigener)
        self.statsBtnBigramVigener.clicked.connect(self.statsBigramVigener)
        self.statsBtnTrigramVigener.clicked.connect(self.statsTrigramVigener)
        self.statsBtnAlphabetVigener.clicked.connect(self.statsLettersVigenerAlphabet)
        
        

    def showSymbolsAlph(self):
        lab1.show_symbols_alphabetical(self.text.toPlainText())
    
    def showSymbolsDesc(self):
        lab1.show_symbols_desc(self.text.toPlainText())

    def showBigrams1(self):
        lab1.show_bigrams(self.text.toPlainText())

    def showTrigrams1(self):
        lab1.show_trigrams(self.text.toPlainText())

    def caesarStatsLetters(self):
        cypher_stats.show_stats_letters(self.encryptedText.toPlainText(), self.uncryptedText.toPlainText())

    def caesarStatsAlphabet(self):
        cypher_stats.show_stats_letters_alphabet(self.encryptedText.toPlainText(), self.uncryptedText.toPlainText())

    def caesarStatsBigram(self):
        cypher_stats.show_stats_bigrams(self.encryptedText.toPlainText(), self.uncryptedText.toPlainText())
    
    def caesarStatsTrigram(self):
        cypher_stats.show_stats_trigrams(self.encryptedText.toPlainText(), self.uncryptedText.toPlainText())

    def statsLettersSZ(self):
        cypher_stats.show_stats_letters(self.encryptedText_SZ.toPlainText(), self.uncryptedText_SZ.toPlainText())

    def statsLettersSZAlphabet(self):
        cypher_stats.show_stats_letters_alphabet(self.encryptedText_SZ.toPlainText(), self.uncryptedText_SZ.toPlainText())

    def statsBigramSZ(self):
        cypher_stats.show_stats_bigrams(self.encryptedText_SZ.toPlainText(), self.uncryptedText_SZ.toPlainText())
    
    def statsTrigramSZ(self):
        cypher_stats.show_stats_trigrams(self.encryptedText_SZ.toPlainText(), self.uncryptedText_SZ.toPlainText())      

    def statsLettersVigener(self):
        cypher_stats.show_stats_letters(self.encryptedText_vigener.toPlainText(), self.unencryptedText_vigener.toPlainText())

    def statsLettersVigenerAlphabet(self):
        cypher_stats.show_stats_letters_alphabet(self.encryptedText_vigener.toPlainText(), self.unencryptedText_vigener.toPlainText())

    def statsBigramVigener(self):
        cypher_stats.show_stats_bigrams(self.encryptedText_vigener.toPlainText(), self.unencryptedText_vigener.toPlainText())
    
    def statsTrigramVigener(self):
        cypher_stats.show_stats_trigrams(self.encryptedText_vigener.toPlainText(), self.unencryptedText_vigener.toPlainText())        
  


    def caesarCypher(self):
        uncrypted = caesar.readCaesarCypher(self.encryptedText.toPlainText(), (int) (self.caesarShift.text()))
        self.uncryptedText.setText(uncrypted)
    

    def szCypher(self):
        changed_alphabet = list()
        colsCount = self.alphabetsTable.columnCount()
        for col in range(0, colsCount):
            twi0 = self.alphabetsTable.item(0, col)
            changed_alphabet.append(twi0.text())
        
        uncrypted = sz.readSZCypher(self.encryptedText_SZ.toPlainText(), changed_alphabet)
        self.uncryptedText_SZ.setText(uncrypted)

    def vigenerEncrypt(self):
        encrypted = vigener.encryptVigener(self.unencryptedText_vigener.toPlainText(), True, self.key.text())
        self.encryptedText_vigener.setText(encrypted)
    
    def vigenerUnencrypt(self):
        unencrypted = vigener.encryptVigener(self.encryptedText_vigener.toPlainText(), False, self.key.text())
        self.unencryptedText_vigener.setText(unencrypted)

def main():
    app = QtWidgets.QApplication(sys.argv) 
    window = LabApp()
    window.show()
    app.exec_()

if __name__ == '__main__': 
    main()  
